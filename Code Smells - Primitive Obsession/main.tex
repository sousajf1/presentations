\documentclass[10pt]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage[english,ngerman]{babel}

% ------------------------------------------------------------------------------
% Use the beautiful metropolis beamer template
% ------------------------------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{FiraSans} 
\mode<presentation>
{
  \usetheme[progressbar=foot,numbering=fraction,background=light]{metropolis} 
  \usecolortheme{crane} % or try albatross, beaver, crane, ...
  \usefonttheme{structurebold}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  %\setbeamertemplate{frame footer}{My custom footer}
} 

% ------------------------------------------------------------------------------
% beamer doesn't have texttt defined, but I usually want it anyway
% ------------------------------------------------------------------------------
\let\textttorig\texttt
\renewcommand<>{\texttt}[1]{%
  \only#2{\textttorig{#1}}%
}

% ------------------------------------------------------------------------------
% minted
% ------------------------------------------------------------------------------
\usepackage{minted}


% ------------------------------------------------------------------------------
% tcolorbox / tcblisting
% ------------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{codecolor}{HTML}{FFC300}

\usepackage{tcolorbox}
\tcbuselibrary{most,listingsutf8,minted}

\tcbset{tcbox width=auto,left=1mm,top=1mm,bottom=1mm,
right=1mm,boxsep=1mm,middle=1pt}

\newtcblisting{myr}[1]{colback=codecolor!5,colframe=codecolor!80!black,listing only, 
minted options={numbers=left, style=tcblatex,fontsize=\tiny,breaklines,autogobble,linenos,numbersep=3mm},
left=5mm,enhanced,
title=#1, fonttitle=\bfseries,
listing engine=minted,minted language=r}


% ------------------------------------------------------------------------------
% Listings
% ------------------------------------------------------------------------------
\definecolor{mygreen}{HTML}{37980D}
\definecolor{myblue}{HTML}{0D089F}
\definecolor{myred}{HTML}{98290D}

\usepackage{listings}

% the following is optional to configure custom highlighting
\lstdefinelanguage{XML}
{
  morestring=[b]",
  morecomment=[s]{<!--}{-->},
  morestring=[s]{>}{<},
  morekeywords={ref,xmlns,version,type,canonicalRef,metr,real,target}% list your attributes here
}

\lstdefinestyle{myxml}{
language=XML,
showspaces=false,
showtabs=false,
basicstyle=\ttfamily,
columns=fullflexible,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
escapeinside={(*@}{@*)},
basicstyle=\color{mygreen}\ttfamily,%\footnotesize,
stringstyle=\color{myred},
commentstyle=\color{myblue}\upshape,
keywordstyle=\color{myblue}\bfseries,
}

% ------------------------------------------------------------------------------
% The Document
% ------------------------------------------------------------------------------
\title{Code Smells - Primitive Obsession}
\author{Jorge Pinto Sousa}
\date{May 2020}
\usemintedstyle[c++]{}
\newcommand{\cpp}{C++}

\begin{document}

\maketitle

\section{Introduction}
\begin{frame}[fragile]{Code Smells}
 \textit{"Code can definitely stink!"}
 \\
 \begin{itemize}

    \item  As Martin Fowler says, a smell is by definition something that's quick to spot (Eg: a long method).
    \item  But it doesn't mean that there's always a real problem with the code. It might indicate a problem, but sometimes, taking the example above, some long methods are just fine.
    \item It is nevertheless a flag that indicates us that we should dig deeper, to understand what's truly going on.
\end{itemize}
\end{frame}
\section{Primitive Obsession }
\begin{frame}[fragile]{Primitive Obsession}
\textbf{What is Primitive Obession?}
\begin{itemize}
    \item  Is an Username a string? Just a string? 
    \item  What about a price? Is it just a double?
    \item  If we do answer \textbf{yes} to the questions above, it's time to think:
      Can a Username usually have whitespaces? \\
      Can a product have a negative price?
    
\end{itemize}
\end{frame}


\begin{frame}[fragile]{Primitive Obsession - Description}
\begin{itemize}
    \item  A lot of classes tend to use primitive types like doubles, integers or strings.
    \item  They actually do exist, but a lot of times their use breaks encapsulation by allowing non-valid values to be assigned to them.
    \item  Briefly the problem resides in the fact that not all the possible values of the primitive type are not valid in the usage context of the class.
    
\end{itemize}
\end{frame}


\begin{frame}[fragile]{Primitive Obsession - Solutions}
\begin{itemize}
    \item  Turn the primitive into an Class. 
    \item  Use that Class to ensure that only valid instances are possible (implement all the validations, guard clauses, etc...).
\end{itemize}
    
    
    (Again this is a code smell, it's not an absolute truth, and doesn't mean that every primitive type usage is equal to a mistake).
\end{frame}


\begin{frame}[fragile]{Primitive Obsession - Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
bool isStringNullOrWhiteSpace(const std::string& s)
{
    return std::find_if(s.begin(),s.end(),
            [](unsigned char c){return !isspace(c);}) == s.end();
}

//...

bool doSomeActionForUserName(const std::string& userName) 
{
    if(isStringNullOrWhiteSpace(userName)){
        std::cout << "Invalid Username" << '\n';
        return false;
    }
    return actionDo(userName);
}

\end{minted}
\end{frame}
\begin{frame}[fragile]{What's wrong?}
\begin{itemize}
    \item Even though there are some business rules associed with the Username (it can't be null, or only composed by whitespaces, or even more complex rules), it's being passed around as a simple std::string.
    \item We need to check for if we are passing a valid value each time we use it!
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Primitive Obsession - Possible Action}

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class Username {
  // ...
  std::string name;
public:
  Username(std::string n) {
      if(!isValid(n)){
          throw std::invalid_argument("invalid username: " + n);
      }else{
          name = std::move(n);
      }
  }

  static bool isValid(const std::string& name){
      return std::find_if(name.begin(),name.end(),
            [](unsigned char c){return isspace(c);}) == name.end();
  }
};
\end{minted}
\end{frame}


\begin{frame}[fragile]{What did we accomplish?}
\begin{itemize}
    \item The business logic is now contained in the Username object itself and not splattered with verifications everywhere else in the code. 
    \item You are sure that every instance of an Username is valid.
\end{itemize}
\end{frame}


\begin{frame}[standout]
    Thank you ~\alert{\faSmileO}~
\end{frame}
\end{document}
