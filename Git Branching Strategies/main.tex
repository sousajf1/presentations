\documentclass[10pt]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage[english,ngerman]{babel}

% ------------------------------------------------------------------------------
% Use the beautiful metropolis beamer template
% ------------------------------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{FiraSans} 
\mode<presentation>
{
  \usetheme[progressbar=foot,numbering=fraction,background=light]{metropolis} 
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  %\setbeamertemplate{frame footer}{My custom footer}
} 

% ------------------------------------------------------------------------------
% beamer doesn't have texttt defined, but I usually want it anyway
% ------------------------------------------------------------------------------
\let\textttorig\texttt
\renewcommand<>{\texttt}[1]{%
  \only#2{\textttorig{#1}}%
}

% ------------------------------------------------------------------------------
% minted
% ------------------------------------------------------------------------------
\usepackage{minted}


% ------------------------------------------------------------------------------
% tcolorbox / tcblisting
% ------------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{codecolor}{HTML}{FFC300}

\usepackage{tcolorbox}
\tcbuselibrary{most,listingsutf8,minted}

\tcbset{tcbox width=auto,left=1mm,top=1mm,bottom=1mm,
right=1mm,boxsep=1mm,middle=1pt}

\newtcblisting{myr}[1]{colback=codecolor!5,colframe=codecolor!80!black,listing only, 
minted options={numbers=left, style=tcblatex,fontsize=\tiny,breaklines,autogobble,linenos,numbersep=3mm},
left=5mm,enhanced,
title=#1, fonttitle=\bfseries,
listing engine=minted,minted language=r}


% ------------------------------------------------------------------------------
% Listings
% ------------------------------------------------------------------------------
\definecolor{mygreen}{HTML}{37980D}
\definecolor{myblue}{HTML}{0D089F}
\definecolor{myred}{HTML}{98290D}

\usepackage{listings}

% the following is optional to configure custom highlighting
\lstdefinelanguage{XML}
{
  morestring=[b]",
  morecomment=[s]{<!--}{-->},
  morestring=[s]{>}{<},
  morekeywords={ref,xmlns,version,type,canonicalRef,metr,real,target}% list your attributes here
}

\lstdefinestyle{myxml}{
language=XML,
showspaces=false,
showtabs=false,
basicstyle=\ttfamily,
columns=fullflexible,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
escapeinside={(*@}{@*)},
basicstyle=\color{mygreen}\ttfamily,%\footnotesize,
stringstyle=\color{myred},
commentstyle=\color{myblue}\upshape,
keywordstyle=\color{myblue}\bfseries,
}

% ------------------------------------------------------------------------------
% The Document
% ------------------------------------------------------------------------------
\title{Branching Strategies}
\author{Luiz Chagas Jardim}
\date{March 2020}
\usemintedstyle[c++]{}
\newcommand{\cpp}{C++}

\begin{document}

\maketitle

\section{Introduction}
\begin{frame}[fragile]{Introduction}
Today we we'll talk about organizing a repository with respect to creating and merging branches.
\pause

Things to think about (not all will be covered here):
\pause
\begin{enumerate}
    \item How often should we create branches?
    \pause
    \item Which branch should we branch from?
    \pause
    \item What should the name of the branch be?
    \pause
    \item How often should we commit/push/pull/rebase?
    \pause
    \item When and with which branch should we merge?
    \pause
    \item Should we squash merge?
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{Notes}
\begin{itemize}
    \item Every project/team is different. There is no right answer. Choose what's best for your situation;
    \pause
    \item This was made with Git in mind, but might apply to other version control software.
\end{itemize}
\end{frame}
\begin{frame}[fragile]{Centralized Workflow}
In this workflow, there is one repository which acts as the central one.
\pause

There is only the master branch and everyone commits to it.
\pause

Advantages:
\pause
\begin{enumerate}
    \item Great way to transition from a centralized control system (such as SVN) to a decentralized control system (such as Git);
    \pause
    \item Very simple workflow and history;
    \pause
    \item Team members can use their own branching strategy in their local repositories, then only push their master branch.
    \pause
\end{enumerate}

Disadvantages:
\pause
\begin{enumerate}
    \item Hard for more than one person to work on the same thing;
    \pause
    \item Your work in progress is not backed up automatically in the remote server.
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{Fork Workflow}
Very similar to the centralized one, but in this case, each contributor or team of contributors make a fork, work on their feature and then make a pull request to the central repository.
\pause

Advantages:
\pause
\begin{enumerate}
    \item Very common in open-source projects, in which anyone can work;
    \pause
    \item Pull requests are a chance for everyone to give feedback.
    \pause
\end{enumerate}

Disadvantages:
\pause
\begin{enumerate}
    \item Does not work well with small, well-integrated teams;
    \pause
    \item Creates a lot of nearly identical repositories in the server.
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{GitHub Flow}
This workflow is described at \href{https://githubflow.github.io/}{githubflow.github.io}
\pause
\begin{enumerate}
    \item Anything in the master branch is deployable;
    \pause
    \item To work on something new, create a descriptively named branch off of master;
    \pause
    \item Commit and push regularly;
    \pause
    \item When you need feedback or help, or you think the branch is ready for merging, open a pull request;
    \pause
    \item After someone else has reviewed and signed off on the feature, you can merge it into master.
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{GitHub Flow}
Advantages:
\pause
\begin{enumerate}
    \item Simple;
    \pause
    \item Used by a lot of companies;
    \pause
    \item Works well with up to 30 members;
    \pause
    \item Works well with CI/CD;
    \pause
    \item Works well with github.
    \pause
\end{enumerate}

Disadvantages:
\pause
\begin{enumerate}[0.]
    \item None that I could find.
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{GitLab Flow}
This workflow is described at \href{https://about.gitlab.com/blog/2014/09/29/gitlab-flow/}{about.gitlab.com/blog/2014/09/29/gitlab-flow}
\pause

This is very similar to GitHub flow, but adapted to the much superior GitLab and its ci/cd.
\end{frame}
\begin{frame}[fragile]{GitFlow}
This workflow has 5 types of branches:
\pause
\begin{enumerate}
    \item Master
    \pause
    \item Release - Used for preparation of planned releases. Merges into Master as tagged commit.
    \pause
    \item Dev
    \pause
    \item Feature - Branches off from development. Used to add a feature.
    \pause
    \item Hotfix - Branches directly from Master to solve a bug.
\end{enumerate}
\end{frame}
\begin{frame}[fragile]{GitFlow}
\begin{figure}
    \centering
    \includegraphics[scale=.25]{gitflow}
\end{figure}
\end{frame}
\begin{frame}[fragile]{DMZ Flow}
This workflow is described at \href{https://gist.github.com/djspiewak/9f2f91085607a4859a66/}{gist.github.com/djspiewak/9f2f91085607a4859a66}
\pause

It is similar to GitFlow, but...
\pause
\begin{enumerate}
    \item There's no Dev branch. Feature branches branch off from Master, or from other feature branches.
    \pause
    \item There's a new branch called DMZ. Feature branches get merged into DMZ. The CI runs on DMZ and, if everything is OK, DMZ is automatically merged into Master.
\end{enumerate}
\end{frame}
\begin{frame}[standout]
    Thank you ~\alert{\faSmileO}~
\end{frame}
\end{document}
