\documentclass[10pt]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage[english,ngerman]{babel}

% ------------------------------------------------------------------------------
% Use the beautiful metropolis beamer template
% ------------------------------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{FiraSans} 
\mode<presentation>
{
  \usetheme[progressbar=foot,numbering=fraction,background=light]{metropolis} 
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  %\setbeamertemplate{frame footer}{My custom footer}
} 

% ------------------------------------------------------------------------------
% beamer doesn't have texttt defined, but I usually want it anyway
% ------------------------------------------------------------------------------
\let\textttorig\texttt
\renewcommand<>{\texttt}[1]{%
  \only#2{\textttorig{#1}}%
}

% ------------------------------------------------------------------------------
% minted
% ------------------------------------------------------------------------------
\usepackage{minted}


% ------------------------------------------------------------------------------
% tcolorbox / tcblisting
% ------------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{codecolor}{HTML}{FFC300}

\usepackage{tcolorbox}
\tcbuselibrary{most,listingsutf8,minted}

\tcbset{tcbox width=auto,left=1mm,top=1mm,bottom=1mm,
right=1mm,boxsep=1mm,middle=1pt}

\newtcblisting{myr}[1]{colback=codecolor!5,colframe=codecolor!80!black,listing only, 
minted options={numbers=left, style=tcblatex,fontsize=\tiny,breaklines,autogobble,linenos,numbersep=3mm},
left=5mm,enhanced,
title=#1, fonttitle=\bfseries,
listing engine=minted,minted language=r}


% ------------------------------------------------------------------------------
% Listings
% ------------------------------------------------------------------------------
\definecolor{mygreen}{HTML}{37980D}
\definecolor{myblue}{HTML}{0D089F}
\definecolor{myred}{HTML}{98290D}

\usepackage{listings}

% the following is optional to configure custom highlighting
\lstdefinelanguage{XML}
{
  morestring=[b]",
  morecomment=[s]{<!--}{-->},
  morestring=[s]{>}{<},
  morekeywords={ref,xmlns,version,type,canonicalRef,metr,real,target}% list your attributes here
}

\lstdefinestyle{myxml}{
language=XML,
showspaces=false,
showtabs=false,
basicstyle=\ttfamily,
columns=fullflexible,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
escapeinside={(*@}{@*)},
basicstyle=\color{mygreen}\ttfamily,%\footnotesize,
stringstyle=\color{myred},
commentstyle=\color{myblue}\upshape,
keywordstyle=\color{myblue}\bfseries,
}

% ------------------------------------------------------------------------------
% The Document
% ------------------------------------------------------------------------------
\title{Test Doubles}
\author{Luiz Chagas Jardim}
\date{March 2020}
\usemintedstyle[c++]{}
\newcommand{\cpp}{C++}

\begin{document}

\maketitle

\section{Introduction}
\begin{frame}[fragile]{What's a test double?}
Test doubles are classes or objects that serve as doubles for the real thing.
\pause

Suppose you are writing automated tests for \lstinline{MyClass}.
\pause

\lstinline{MyClass} depends on the classes \lstinline{Dependendy1}, \lstinline{Dependendy2} and \lstinline{Dependendy3}, through arguments in the constructor, arguments in member functions or template arguments.
\pause

Maybe you can use test doubles for these dependencies.
\pause

If you have a dependency such as a member variable that is not passed as a parameter, maybe you need \emph{dependency injection}, but this is another topic.
\end{frame}
\begin{frame}[fragile]{Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class MyClass {
    explicit MyClass(Dependency1 const& d1) {
        //...
    }
    bool foo(Dependency2 const& d2) {
        //...
    }
    int bar(int i, Dependency3 const& d3) {
        //...
    }
};

TEST {
    MyClass instance(dependency1_double);
    EXPECT_TRUE(instance.foo(dependency2_double));
    EXPECT_EQ(instance.bar(0, dependency3_double));
}
\end{minted}
\end{frame}
\begin{frame}[fragile]{Why use test doubles?}
\begin{itemize}
    \item When a test for \lstinline{MyClass} fails, you know that the error comes from \lstinline{MyClass}, and not one of its dependencies;\pause
    \item For performance reasons, you might want to use a simplified version of a dependency;\pause
    \item Dependencies such as databases that change with time or random number generators will make the test non-deterministic.
\end{itemize}
\end{frame}

\section{Test Double Types}
\section{Dummy}
\begin{frame}[fragile]{Dummy}
A dummy is an object that is not used and not meant to be used.
\end{frame}
\begin{frame}[fragile]{Dummy Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class TripleBool {
public:
    TripleBool(bool b1, bool b2, bool b3) :
        b1(b1), b2(b2), b3(b3) {}
    TripleBool operator&&(TripleBool other) {    
        return TripleBool(b1 && other.b1,
            b2 = b2 || other.b2, b3 = !other.b3);
    }
private:
    bool b1, b2, b3;
};

class Class {
public:
    Class(int i, TripleBool t) : i(i), t(t) {}
    int increment() {
        return ++i;
    }
    TripleBool And(TripleBool otherT) {
        return t = t && otherT;
    }
private:
    int i;
    TripleBool t;
};
\end{minted}
\end{frame}

\begin{frame}[fragile]{Dummy Example}
Suppose we want to test the \lstinline{Class::increment()} member function.
\pause

This function does not use \lstinline{TripleBool}, but we need a \lstinline{TripleBool} to instantiate \lstinline{Class}.
\pause

So we create a \emph{dummy} for \lstinline{TripleBool}.
\pause

To make it extra clear, we will write a \lstinline{Dummy} class which extends from \lstinline{TripleBool} and fills it with irrelevant values for us. This may be overkill for this example, but imagine a more complex one.
\end{frame}

\begin{frame}[fragile]{Dummy Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class DummyTripleBool : public TripleBool {
public:
    DummyTripleBool() : TripleBool(true, false, false) {}
};

TEST {
    DummyTripleBool triple_bool;
    Class instance(17, triple_bool);
    assert(instance.increment() == 18);
    assert(instance.increment() == 19);
    assert(instance.increment() == 20);
}
\end{minted}
\end{frame}
\begin{frame}[fragile]{Dummy Use Cases}
\begin{itemize}
    \item When you have a parameter whose value is irrelevant for the test.
\end{itemize}
\end{frame}
\section{Fake}
\begin{frame}[fragile]{Fake}
Fakes have a working implementation that is different from the actual implementation (usually a simplification).
\pause

For example, instead of accessing a database and returning query results, a fake might run the query on a small hardcoded in-memory database meant for testing.
\end{frame}
\begin{frame}[fragile]{Fake Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct DatabaseReader {
    int getAgeFromId(int id) {
        //access database
        return age;
    }
    std::string getNameFromId(int id) {
        //access database
        return name;
    }
};

struct FakeDatabaseReader {
    int getAgeFromId(int id) { return ages.at(id); }
    std::string getNameFromId(int id) { return names.at(id); }
private:
    std::vector<int> ages{4, 11, 2, 4};
    std::vector<std::string> names{"Gamora", "Kawaii",
        "Serena", "Pluto"};
};

template <typename DbReader = DatabaseReader>
void FunctionToBeTested(int id) {
    //uses dbreader member functions
};
\end{minted}
\end{frame}
\begin{frame}[fragile]{Fake Example}
This is a very crude example, but you can get the idea. If you call \lstinline{FunctionToBeTested}, it will access the database. But in a test you can call \lstinline{FunctionToBeTested<DatabaseReader>} and it will use the hard-coded database.
\pause

You could even provide a constructor for the Fake class which receives other values for the ages and names.
\end{frame}
\begin{frame}[fragile]{Fake Example}
The dependency injection was made using a default template argument, which is good because there's no runtime penalty. Use a concept or ad-hoc constraints in C++20 to keep the classes in sync.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
template <typename DbReader = DatabaseReader>
    requires requires (DbReader dbReader) {
        dbReader.getAgeFromId(0);
        dbReader.getNameFromId(0);
    }
void FunctionToBeTested(int id) {
    //uses dbreader member functions
};
\end{minted}
\end{frame}
\begin{frame}[fragile]{Fake Use Cases}
\begin{enumerate}
    \item Unit testing if no mocks are provided;
    \pause
    \item As a placeholder when the actual class is not available. This is very useful in Outside-In Test Driven Development;
    \pause
    \item To remove randomness, a fake may be used instead of a random number generator;
    \pause
    \item Databases can change with time and make tests fail. Use a fake to simulate tables.
\end{enumerate}
\end{frame}
\section{Stub}
\begin{frame}[fragile]{Stub}
Stubs are similar to Fake, but instead of being implemented with a shortcut, it outright returns a fixed answer.
\pause

Just like with a Fake, you can provide methods to set these answers.
\end{frame}
\begin{frame}[fragile]{Stub Example}
Let's use the same code as the Fake example.
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct StubDatabaseReader {
    int getAgeFromId(int id) { return 0; }
    std::string getNameFromId(int id) { return "name"; }
};
\end{minted}
\end{frame}
\section{Spy}
\begin{frame}[fragile]{Spy}
A Spy is a test double that can observe how many times a method was called.
\pause

This can then be used to verify behavior in a test.
\end{frame}
\begin{frame}[fragile]{Spy Example}
In the example below, we want to test that \lstinline{function_to_be_tested} calls \lstinline{foo} four times:
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct A {
    explicit A(int i) {
        //do something
    }
    void foo() const {
        //do something
    }
};

void function_to_be_tested() {
    //...
    A a(0);
    for (int i = 0; i < 4; i++) a.foo();
    //...
}
\end{minted}
\end{frame}
\begin{frame}[fragile]{Spy Example}
To make it testable, we will refactor the code to use dependency injection (through a default template argument \emph{and} through a default argument):
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
template<typename T = A>
void function_to_be_tested(T const& t = T(0)) {
    //...
    for (int i = 0; i < 4; i++) t.foo();
    //...
}
\end{minted}
\pause
Instead, we could have made \lstinline{foo} and A's destructor virtual, then extended A. The template solution offers no runtime penalty.
\end{frame}
\begin{frame}[fragile]{Spy Example}
Now we can make a very simple Spy for A:
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct SpyA {
    explicit SpyA(int i) : a(i) {}
    void foo() const {
        foo_times_called++;
        a.foo();
    }
    unsigned short get_foo_times_called() const {
        return foo_times_called;
    }
private:
    A a;
    unsigned short mutable foo_times_called = 0;
};
\end{minted}
\pause
Now our test can assert that \lstinline{foo} get called four times by calling \lstinline{function_to_be_tested(SpyA(0))}. The compiler deduces the template argument for us.
\end{frame}
\begin{frame}[fragile]{Spy Use Cases}
\begin{enumerate}
    \item When a mock is not provided;
    \pause
    \item To test behaviour instead of state, or when you want to test behaviour \emph{and} state.
\end{enumerate}
\pause
State testing: When I call this member function with these values, I expect to see this value returned.
\pause

Behaviour testing: When I call this member function with theses value, I expect that this other member function will be called, then this other will be called twice.
\end{frame}
\section{Mock}
\begin{frame}[fragile]{Mock}
A mock is the ultimate double. It is usually implemented through a library such as gmock for C++.
\pause
\begin{enumerate}
    \item Like a spy, a mock counts how many times a member function is called. But with a mock the test must specify that that function should be called, optionally specifying how many times it will be called. The mock will make the test fail if this is not correct.
    \pause
    \item Like a stub, the test can set what the mocked member function will return each time it is called.
\end{enumerate}
\pause
Mock usage and best practices are a long topic that we'll leave for another time. We'll only say that it is best practice for the mock to be provided by the same library that provides the implementation.
\end{frame}
\section{Final Notes}
\begin{frame}[fragile]{Summary of Test Double Types Part 1}
\begin{tabular}{|p{\textwidth/8}|p{\textwidth/4}|p{\textwidth/2}|}
    \hline
    Double & Definition & Use cases \\
    \hline\hline
    Dummy & An object that is not used. & When you must create an object but it's value doesn't matter. \\
    \hline
    Fake & A simplified version of an object. May be programmable. &
\begin{itemize}
    \item To write unit tests without a mock;
    \item To make tests faster;
    \item To avoid stochastic or time dependent results.
\end{itemize} \\
    \hline
    Stub & Returns a fixed value. This value may be programmable. & Same as Fake.\\
    \hline
\end{tabular}
\end{frame}
\begin{frame}[fragile]{Summary of Test Double Types Part 2}
\begin{tabular}{|p{\textwidth/8}|p{\textwidth/4}|p{\textwidth/2}|}
    \hline
    Double & Definition & Use cases \\
    \hline\hline
    Spy & Records how many times a function gets called. &
\begin{itemize}
    \item To write unit tests without a mock;
    \item When you want to test behaviour and/instead of state.
\end{itemize} \\
    \hline
    Mock & Stub + Spy + ability to fail test if call expectations are not satisfied & Same as stub and spy\\
    \hline
\end{tabular}
\end{frame}
\begin{frame}[fragile]{Don't worry too much}
Don't worry too much about classifying test doubles. The definitions are inconsistent across the literature, but some people will still fight to the death over them.
\pause

In practice, you will find that some doubles are actually a mix of them. More important than getting the classification exactly right is to have good code.
\end{frame}
\begin{frame}[standout]
    Thank you ~\alert{\faSmileO}~
\end{frame}
\end{document}
